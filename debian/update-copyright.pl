#! /usr/bin/perl -w

use Debian::Copyright;
use File::Find;
use Data::Dumper;

my %files;
my %info;

my @seps = (
    'and',
    '&',
    'with input from',
    'updates by',
    'with edits by',
    'based on work by',
    'adapted by',
);
my %authormap = (
    'E.* (B\w*.? )?Knudsen' => 'Erik Bergbäck Knudsen <erkn@fysik.dtu.dk>',
    'E.* Farhi'      => 'Emmanuel Farhi <emmanuel.farhi@synchrotron-soleil.fr>',
    'Farhi Emmanuel' => 'Emmanuel Farhi <emmanuel.farhi@synchrotron-soleil.fr>',
    'Martin .* Pedersen' => 'Martin Cramer Pedersen <mcpe@nbi.dk>',
    'S.* Kynde'          => 'Søren Kynde <kynde@nbi.dk>',
    'Will?endrup'        => 'Peter Willendrup <pkwi@fysik.dtu.dk>',
    'Andreas Ostermann'  => 'Andreas Ostermann <Andreas.Ostermann@frm2.tum.de>',
    'A.* Padovani'       => 'Antoine Padovani',
    'Lefmann'            => 'Kim Lefmann <kim.lefmann@risoe.dk>',
    'vickery'            => 'Anette Vickery <anette.vickery@fys.ku.dk>',
    'Daniel Lomholt Christensen' =>
      'Daniel Lomholt Christensen <dlc@math.ku.dk>',
    'A.*Abrahamsen'       => 'A. B. Abrahamsen',
    'Duc Le'              => 'Duc Le <duc.le@stfc.ac.uk>',
    'Klinkby'             => 'Esben Klinkby <esbe@dtu.dk>',
    'Schoe?ber'           => 'Helmuth Schoeber',
    'Jana Baltser'        => 'Jana Baltser <jana.baltser@fys.ku.dk>',
    'Saroun'              => 'Jan Saroun <saroun@ujf.cas.cz>',
    'Jose.*Robledo'       => 'Jose I. Robledo',
    'Kristian Soe?rensen' => 'Kristian Soerensen',
    'Mendenhall' => 'Marcus H. Mendenhall <marcus.mendenhall@nist.gov>',
    'NIST <marcus.mendenhall@nist.gov>' =>
      'Marcus H. Mendenhall <marcus.mendenhall@nist.gov>',
    'Linda Udby'         => 'Linda Udby <udby@fys.ku.dk>',
    'Peter Christiansen' => 'Peter Christiansen <peter.christiansen@risoe.dk>',
    'Peter Link'         => 'Peter Link <plink@physik.tu-muenchen.de>',
    'Philip Smith'       => 'Philip Smith <s154443@win.dtu.dk>',
    'Robert Dalgliesh'   => 'Robert Dalgliesh <robert.dalgliesh@stfc.ac.uk>',
    'Tobias Panzner'     => 'Tobias Panzner <tobias.panzner@psi.ch>',
    'Uwe Filges'         => 'Uwe Filges <uwe.filges@psi.ch>',
    'A. Prodi'           => 'Andrea Prodi',
    'Desiree D. M. Ferreira' => 'Desiree D. M. Ferreira <desiree@space.dtu.dk>',
    'Dickon Champion'        => 'Dickon Champion <d.champion@rl.ac.uk>',
    'E.*Lauridsen'           => 'E. M. Lauridsen',
    'Lieutenant'             => 'Klaus Lieutenant',
    'K. Nielsen'             => 'Kristian Nielsen',
);

sub wanted {
    /\.(instr|comp)/ || return;

    my $f = $File::Find::name;

    open my $fh, $_ or die $!;
    while ( my $l = <$fh> ) {
        chomp $l;
        next unless $l =~ /Written by ?:(.*)/;
        my $authors = $1;
        $authors =~ s/,? & /, /g;
        foreach my $s (@seps) {
            $authors =~ s/,? $s /, /g;
        }
        $authors =~ s|(?<!)/|,|g;    # zero-width negative lookbehind assertion
        $authors =~ s/^(.)\. ?(.*)/$1. $2/g;
        my @authors;
        foreach my $a ( split /, */, $authors ) {
            $a =~ s/<a href="(mailto[: ]?)?(.*?)">(.*?)<.*>/$3 <$2>/;
            foreach my $r ( keys %authormap ) {
                $a =~ s/.*$r.*/$authormap{$r}/i;
            }
            $a =~ s/\s+$//;
            $a =~ s/^\s+//;
            push @authors, $a;
        }

        foreach my $a (@authors) {

            # print "$a\n";
        }
        $authors = join ", ", sort @authors;
        $f =~ s|^\./||;
        push @{ $files{$authors} }, $f if $f;

        # print "$authors\n";
    }
}

find( \&wanted, ('.') );

my $fn = 'debian/copyright';
my $c  = Debian::Copyright->new();
my %seen;
$c->read($fn);
foreach my $f ( $c->files ) {
    foreach my $s ( $f->Values ) {
        foreach my $x ( grep { /./ } split( /\s+/s, $s->FETCH('Files') ) ) {
            $seen{$x} = 1;
        }
    }
}

foreach my $a ( sort keys %files ) {
    my $files =
      join( "\n ", sort grep { !$seen{$_} } grep { /./ } @{ $files{$a} } );
    next unless $files;
    $files = "\n " . $files;
    my $fs = Debian::Copyright::Stanza::Files->new(
        {
            Files     => $files,
            Copyright => "\n " . join( "\n ", sort split( /, /, $a ) ),
            License   => 'GPL-3',
        }
    );
    $c->files->Push( $a => $fs );
}

$c->write("${fn}");
system "sed -i -e 's/: \$/:/' ${fn}"
