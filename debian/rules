#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

# INSTALL_PREFIX=/usr/share
INSTALL_PREFIX=/usr/
MCCODE_VERSION=3
MCVERSION=3
MCSTAS_VERSION=3.5
MCXTRACE_VERSION=3.5

# WARNING: the MCCODE_USE_LEGACY_DESTINATIONS=OFF changes the legacy tools/library destination to 'resources'
CMAKE_ARGS=-DMCCODE_VERSION=$(MCCODE_VERSION) -DCMAKE_INSTALL_PREFIX=$(INSTALL_PREFIX) \
  -DMCCODE_USE_LEGACY_DESTINATIONS=OFF -DENABLE_CIF2HKL=OFF
CMAKE_ARGS_N=-Denable_mcstas=1   -DBUILD_MCSTAS=1   $(CMAKE_ARGS) -DMCVERSION=$(MCSTAS_VERSION)
CMAKE_ARGS_X=-Denable_mcxtrace=1 -DBUILD_MCXTRACE=1 $(CMAKE_ARGS) -DMCVERSION=$(MCXTRACE_VERSION)

PARALLEL=$(shell echo $DEB_BUILD_OPTIONS|xargs -n 1|grep ^parallel=|cut -d= -f2)
ifneq ($(strip $(PARALLEL)),)
	MAKE_ARGS=-j$(PARALLEL)
endif

# compile function: mccode_compile_N $pkg $src_cmake_dir
define mccode_compile_N
	# compile $(2)
	mkdir -p BUILD/$(1) && cd BUILD/$(1) && cmake $(CMAKE_ARGS_N) $(CURDIR)/$(2) && make $(MAKE_ARGS)
endef

# compile function: mccode_compile_X $pkg $src_cmake_dir
define mccode_compile_X
	# compile $(2)
	mkdir -p BUILD/$(1) && cd BUILD/$(1) && cmake $(CMAKE_ARGS_X) $(CURDIR)/$(2) && make $(MAKE_ARGS)
endef

# install function: mccode_install $pkg
define mccode_install
	# installing $(1)
	cd BUILD/$(1) && make install DESTDIR=$(CURDIR)/debian/$(1)
	# install: make executables for $(1)
	if [ -d $(CURDIR)/debian/$(1)/usr/bin ]; then \
	  chmod +x $(CURDIR)/debian/$(1)/usr/bin/*; fi
	# create man pages for $(1) if any
	for i in $$(ls $(CURDIR)/debian/$(1)/usr/share/doc/mcstas/*.md \
	  $(CURDIR)/debian/$(1)/usr/share/doc/mcxtrace/*.md); do \
	    mkdir -p $(CURDIR)/debian/$(1)/usr/share/man/man1/; \
	    bn=$$(basename -s .md $$i); \
	    pandoc --standalone --to man $$i -o $(CURDIR)/debian/$(1)/usr/share/man/man1/$${bn}.1; \
	done
endef

%:
	dh $@ --with cmake

execute_after_dh_auto_clean:
	rm -f cmake/Modules/debian.cmake

override_dh_auto_configure:

override_dh_auto_build:
	mkdir -p BUILD
	cp cmake/toolchains/deb64.cmake cmake/Modules/debian.cmake
	sed -i -e /amd64/d -e/CMAKE_SYSTEM_NAME/d cmake/Modules/debian.cmake

	# Performing mcstas build
	$(call mccode_compile_N,mcstas,mcstas)

	$(call mccode_compile_N,mcstas-comps,mcstas-comps)
	$(call mccode_compile_N,mcstas-mccodelib,tools/Python/mccodelib/)
	$(call mccode_compile_N,mcstas-mcdoc,tools/Python/mcdoc)
	$(call mccode_compile_N,mcstas-manuals,docpkg/manuals/mcstas)
	$(call mccode_compile_N,mcstas-mcrun,tools/Python/mcrun)
	$(call mccode_compile_N,mcstas-mcgui,tools/Python/mcgui)
	$(call mccode_compile_N,mcstas-clusterscripts,tools/cluster-scripts)

	$(call mccode_compile_N,mcstas-mcplot-pyqtgraph,tools/Python/mcplot/pyqtgraph)
	$(call mccode_compile_N,mcstas-mcplot-matplotlib,tools/Python/mcplot/matplotlib)
	$(call mccode_compile_N,mcstas-mcplot-html,tools/Python/mcplot/html)
	$(call mccode_compile_N,mcstas-mcplot-matlab,tools/matlab/mcplot)

	$(call mccode_compile_N,mcstas-mcdisplay-webgl,tools/Python/mcdisplay/webgl)
	$(call mccode_compile_N,mcstas-mcdisplay-webgl-classic,tools/Python/mcdisplay/webgl-classic)
	$(call mccode_compile_N,mcstas-mcdisplay-pyqtgraph,tools/Python/mcdisplay/pyqtgraph)
	$(call mccode_compile_N,mcstas-mcdisplay-matplotlib,tools/Python/mcdisplay/matplotlib/)
	$(call mccode_compile_N,mcstas-mcdisplay-matlab,tools/matlab/mcdisplay)

	$(call mccode_compile_N,mcstas-mcdisplay-mantid,tools/Python/mcdisplay/mantid_xml)
	$(call mccode_compile_N,mcstas-mcresplot,tools/Python/mcresplot)

	# Performing mcxtrace build
	$(call mccode_compile_X,mcxtrace,mcxtrace)

	$(call mccode_compile_X,mcxtrace-comps,mcxtrace-comps)
	$(call mccode_compile_X,mcxtrace-mccodelib,tools/Python/mccodelib)
	$(call mccode_compile_X,mcxtrace-mxrun,tools/Python/mcrun)
	$(call mccode_compile_X,mcxtrace-mxgui,tools/Python/mcgui)
	$(call mccode_compile_X,mcxtrace-clusterscripts,tools/cluster-scripts)
	$(call mccode_compile_X,mcxtrace-mxdoc,tools/Python/mcdoc)
	$(call mccode_compile_X,mcxtrace-manuals,docpkg/manuals/mcxtrace)

	$(call mccode_compile_X,mcxtrace-mxplot-pyqtgraph,tools/Python/mcplot/pyqtgraph)
	$(call mccode_compile_X,mcxtrace-mxplot-matplotlib,tools/Python/mcplot/matplotlib)
	$(call mccode_compile_X,mcxtrace-mxplot-html,tools/Python/mcplot/html)
	$(call mccode_compile_X,mcxtrace-mxplot-matlab,tools/matlab/mcplot)

	$(call mccode_compile_X,mcxtrace-mxdisplay-webgl,tools/Python/mcdisplay/webgl)
	$(call mccode_compile_X,mcxtrace-mxdisplay-webgl-classic,tools/Python/mcdisplay/webgl-classic)
	$(call mccode_compile_X,mcxtrace-mxdisplay-pyqtgraph,tools/Python/mcdisplay/pyqtgraph)
	$(call mccode_compile_X,mcxtrace-mxdisplay-matplotlib,tools/Python/mcdisplay/matplotlib/)
	$(call mccode_compile_X,mcxtrace-mxdisplay-matlab,tools/matlab/mcdisplay)


execute_before_dh_auto_install:
	# Performing mcstas install in packages
	$(call mccode_install,mcstas)

	$(call mccode_install,mcstas-comps)
	$(call mccode_install,mcstas-mccodelib)
	$(call mccode_install,mcstas-mcdoc)
	$(call mccode_install,mcstas-manuals)
	$(call mccode_install,mcstas-mcrun)
	$(call mccode_install,mcstas-mcgui)
	$(call mccode_install,mcstas-clusterscripts)
	$(call mccode_install,mcstas-mcplot-pyqtgraph)
	$(call mccode_install,mcstas-mcplot-matplotlib)
	$(call mccode_install,mcstas-mcplot-matlab)
	$(call mccode_install,mcstas-mcplot-html)
	$(call mccode_install,mcstas-mcdisplay-webgl)
	$(call mccode_install,mcstas-mcdisplay-webgl-classic)
	$(call mccode_install,mcstas-mcdisplay-pyqtgraph)
	$(call mccode_install,mcstas-mcdisplay-matplotlib)
	$(call mccode_install,mcstas-mcdisplay-mantid)
	$(call mccode_install,mcstas-mcdisplay-matlab)
	$(call mccode_install,mcstas-mcresplot)
	# Ignored: (mcstas-ncrystal)

	# Performing mcxtrace install in packages
	$(call mccode_install,mcxtrace)

	$(call mccode_install,mcxtrace-comps)
	$(call mccode_install,mcxtrace-mccodelib)
	$(call mccode_install,mcxtrace-clusterscripts)
	$(call mccode_install,mcxtrace-manuals)
	$(call mccode_install,mcxtrace-mxdoc)
	$(call mccode_install,mcxtrace-mxrun)
	$(call mccode_install,mcxtrace-mxgui)
	$(call mccode_install,mcxtrace-mxplot-matplotlib)
	$(call mccode_install,mcxtrace-mxplot-pyqtgraph)
	$(call mccode_install,mcxtrace-mxplot-html)
	$(call mccode_install,mcxtrace-mxplot-matlab)
	$(call mccode_install,mcxtrace-mxdisplay-webgl)
	$(call mccode_install,mcxtrace-mxdisplay-webgl-classic)
	$(call mccode_install,mcxtrace-mxdisplay-pyqtgraph)
	$(call mccode_install,mcxtrace-mxdisplay-matplotlib)
	$(call mccode_install,mcxtrace-mxdisplay-matlab)

	# Replace symlinks with the files they point to
	for i in $$(awk '/^Package:/ { print $$2 }' debian/control) ; do\
	  find debian/$$i -type l -print0 | xargs -r  -0 sed -i -e '' ; \
	done

execute_after_dh_auto_install:
	# avoid conflicts and lintian warnings
	mv debian/mcstas/usr/share/mcstas/mcstas-environment        debian/mcstas/usr/bin/
	mv debian/mcxtrace/usr/share/mcxtrace/mcxtrace-environment  debian/mcxtrace/usr/bin/
	chmod +x debian/mcxtrace-comps/usr/share/mcxtrace/resources/data/get_xray_db_data

	# module files
	mkdir -p debian/mcxtrace/etc/modulefiles
	mkdir -p debian/mcstas/etc/modulefiles
	mv debian/mcxtrace/usr/share/mcxtrace/mcxtrace-module debian/mcxtrace/etc/modulefiles/
	mv debian/mcstas/usr/share/mcstas/mcstas-module       debian/mcstas/etc/modulefiles/
	chmod -x debian/mcxtrace/etc/modulefiles/* debian/mcstas/etc/modulefiles/*

	# reproducible build
	find debian -name mccode_config.json -print0 | xargs -0rt sed -i -e "s@$(CURDIR)@/builddir@g"

	# add editor highlight syntaxing
	mkdir -p debian/mcstas-comps/usr/share/gtksourceview-4/language-specs/
	mkdir -p debian/mcxtrace-comps/usr/share/gtksourceview-4/language-specs/
	ln -s /usr/share/mcstas/editors/mccode.lang   debian/mcstas-comps/usr/share/gtksourceview-4/language-specs/mcstas.lang
	ln -s /usr/share/mcxtrace/editors/mccode.lang debian/mcxtrace-comps/usr/share/gtksourceview-4/language-specs/mcxtrace.lang

	# Remove JavaScript files as will use those from system
	rm -f debian/mcstas-mcdisplay-webgl-classic/usr/share/mcstas/tools/Python/mcdisplay/webgl-classic/jquery.min.js
	rm -f debian/mcstas-mcdisplay-webgl-classic/usr/share/mcstas/tools/Python/mcdisplay/webgl-classic/three.min.js
	rm -f debian/mcstas-mcdisplay-webgl-classic/usr/share/mcstas/tools/Python/mcdisplay/webgl-classic/dat.gui.min.js
	rm -f debian/mcxtrace-mxdisplay-webgl-classic/usr/share/mcxtrace/tools/Python/mxdisplay/webgl-classic/jquery.min.js
	rm -f debian/mcxtrace-mxdisplay-webgl-classic/usr/share/mcxtrace/tools/Python/mxdisplay/webgl-classic/three.min.js
	rm -f debian/mcxtrace-mxdisplay-webgl-classic/usr/share/mcxtrace/tools/Python/mxdisplay/webgl-classic/dat.gui.min.js

	# Clean remaining gitignore files
	rm -f debian/mcstas-comps/usr/share/mcstas/resources/obsolete/.gitignore
	rm -f debian/mcstas-comps/usr/share/mcstas/resources/sasmodels/.gitignore
	rm -f debian/mcxtrace-comps/usr/share/mcxtrace/resources/sasmodels/.gitignore

	# Generate docs, update path to html pages in overview, and store in -doc packages
	PYTHONPATH=$(CURDIR)/debian/mcstas-mccodelib/usr/share/mcstas/tools/Python\
	  debian/mcstas-mcdoc/usr/bin/mcdoc -i --dir $(CURDIR)/debian/mcstas-comps/usr/share/mcstas/resources/
	sed -i "s|$(CURDIR)/debian/mcstas-mccodelib||g" $(CURDIR)/debian/mcstas-mccodelib/usr/share/doc/mcstas/mcdoc.html
	mkdir -p $(CURDIR)/debian/mcstas-comps/usr/share/doc/mcstas
	mv $(CURDIR)/debian/mcstas-mccodelib/usr/share/doc/mcstas/mcdoc.html $(CURDIR)/debian/mcstas-comps/usr/share/doc/mcstas

	PYTHONPATH=$(CURDIR)/debian/mcxtrace-mccodelib/usr/share/mcxtrace/tools/Python\
	  debian/mcxtrace-mxdoc/usr/bin/mxdoc -i --dir $(CURDIR)/debian/mcxtrace-mxdoc/usr/share/mcxtrace/resources/
	sed -i "s|$(CURDIR)/debian/mcxtrace-mccodelib||g" $(CURDIR)/debian/mcxtrace-mccodelib/usr/share/doc/mcxtrace/mxdoc.html
	mkdir -p $(CURDIR)/debian/mcxtrace-comps/usr/share/doc/mcxtrace
	mv $(CURDIR)/debian/mcxtrace-mccodelib/usr/share/doc/mcxtrace/mxdoc.html $(CURDIR)/debian/mcxtrace-comps/usr/share/doc/mcxtrace

	# Byte-compile Python files at install time
	dh_python3 -p mcstas-mccodelib                  /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcdisplay-mantid           /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcdisplay-matplotlib       /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcdisplay-pyqtgraph        /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcdisplay-webgl            /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcdisplay-webgl-classic    /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcdoc                      /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcgui                      /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcplot-pyqtgraph           /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcresplot                  /usr/share/mcstas/tools/Python
	dh_python3 -p mcstas-mcrun                      /usr/share/mcstas/tools/Python
	dh_python3 -p mcxtrace-mccodelib                /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxdisplay-matplotlib     /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxdisplay-pyqtgraph      /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxdisplay-webgl          /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxdisplay-webgl-classic  /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxdoc                    /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxgui                    /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxplot-pyqtgraph         /usr/share/mcxtrace/tools/Python
	dh_python3 -p mcxtrace-mxrun                    /usr/share/mcxtrace/tools/Python


execute_after_dh_gencontrol:
	set -e ; for i in $$(awk '/Package: mcstas/ { print $$2 }'   debian/control) ; do \
	  dh_gencontrol -p$$(basename $$i) -- -v$(MCSTAS_VERSION)+$(DEB_VERSION) ; \
	done
	set -e ; for i in $$(awk '/Package: mcxtrace/ { print $$2 }' debian/control) ; do \
	  dh_gencontrol -p$$(basename $$i) -- -v$(MCXTRACE_VERSION)+$(DEB_VERSION) ; \
	done
